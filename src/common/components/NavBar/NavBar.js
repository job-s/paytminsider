import React from "react";
import { Link } from 'react-router-dom';
import { isAccessGranted } from "../../utils/authUtils";
import "./navBar.scss";

export const NavBar = () => {
  return (
    <nav className="nav-bar">
      {isAccessGranted() && <>
        <Link className="nav-button" to="/upload">Upload Photo</Link>
        <Link className="nav-button" to="/viewAlbums">View Albums</Link>
      </>}
    </nav>
  );
}