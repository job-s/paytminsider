import React from "react";

import "./fileInput.scss";

export const FileInput = ({ _id, onChange }) => {
  return (
    <div className="file-input-container">
      <label className="file-input-label" htmlFor={_id}>Choose File</label>
      <input id={_id} className="file-input" type="file" name="files" defaultValue="" onChange={onChange} />
    </div>
  );
}