export const uploadHeaders = {
  Authorization: `Bearer ${localStorage.getItem("access_token")}`,
  "Content-type": "application/octet-stream",
  "X-Goog-Upload-Content-Type": "image/jpeg",
  "X-Goog-Upload-Protocol": "raw"
}

export const authoricationHeader = {
  Authorization: `Bearer ${localStorage.getItem("access_token")}`
}