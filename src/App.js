import React from 'react';
import { Router, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from "history";
import { Upload } from "./pages/upload/Upload";
import { ViewAlbums } from './pages/viewAlbums/ViewAlbums';
import { ViewPhoto } from './pages/viewPhoto/ViewPhoto';
import { GrantAccess } from "./pages/GrantAccess/grantAccess";
import { PrivateRoute } from "./common/components/Routes/PrivateRoute";
import { PublicRoute } from "./common/components/Routes/PublicRoute";
import './App.css';

const customHistory = createBrowserHistory();

function App() {
  return (
    <div className="App">
      <Router history={customHistory}>
        <div className="App">
          <Switch>
            <PublicRoute path="/grantAccess" component={GrantAccess} redirectPath="/upload" />
            <PrivateRoute path="/upload" component={Upload} redirectPath="/grantAccess" />
            <PrivateRoute path="/viewAlbums" component={ViewAlbums} redirectPath="/grantAccess" />
            <PrivateRoute path="/photo" component={ViewPhoto} redirectPath="/grantAccess" />
            <Redirect to="/upload" />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
