import React from "react";
import GoogleLogin from "react-google-login";
import { NavBar } from "../../common/components/NavBar/NavBar";
import "./grantAccess.scss";

export const GrantAccess = (props) => {

  const responseGoogle = (response) => {
    if (response?.tokenObj?.access_token) {
      localStorage.setItem("access_token", response.tokenObj.access_token);
      props.history.push("/upload");
    }
  }

  return (
    <div>
      <NavBar />
      <p>Grant access to your google photos to continue.</p>
      <br />
      <GoogleLogin
        clientId={process.env.REACT_APP_CLIENT_ID}
        buttonText="Grant Access"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={"single_host_origin"}
        scope="https://www.googleapis.com/auth/photoslibrary"
      />

    </div>
  );
}