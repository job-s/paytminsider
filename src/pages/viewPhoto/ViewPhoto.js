import React, { useEffect, useState } from 'react';
import { NavBar } from "../../common/components/NavBar/NavBar";
import { authoricationHeader } from "../../common/utils/headers";
import "./viewPhoto.scss";

export const ViewPhoto = (props) => {
  const [albumContents, setAlbumContents] = useState([]);

  useEffect(() => {
    fetch("https://photoslibrary.googleapis.com/v1/mediaItems:search", {
      method: "POST",
      headers: authoricationHeader,
      body: JSON.stringify({
        pageSize: "4",
        albumId: props?.location?.state?.albumId
      })
    }).then(response => response.json())
      .then(response => setAlbumContents(response.mediaItems || []));
  }, [props]);

  return (
    <div>
      <div><NavBar /></div>
      <div className="album-contents">
        {
          albumContents.map((item, index) => {
            const { width, height } = item.mediaMetadata;
            return (
              <div key={index} style={{ width: width + "px", height: Number(height) + 20 + "px" }}>
                <img src={item.baseUrl} alt="album contents" style={{ width: width + "px", height: height + "px" }} />
                <p>{width} x {height}</p>
              </div>
            )
          })
        }
      </div>
    </div>
  );
}

