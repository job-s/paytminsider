import React, { useState, useRef } from 'react';
import { v4 as uuidv4 } from "uuid";
import { NavBar } from "../../common/components/NavBar/NavBar";
import { ImageComponent } from "./common/components/ImageComponent/ImageComponent";
import { CropImage } from "./common/components/CropImage/CropImage";
import { sizeMap, defaultCrop, CANVAS_LENGTH } from "./common/constants/constants";
import { FileInput } from '../../common/components/FileInput/FileInput';
import { uploadHeaders, authoricationHeader } from "../../common/utils/headers";
import "./upload.scss";

export const Upload = (props) => {
  const [size, setSize] = useState("h");
  const [croppedDataUrl, setCroppedDataUrl] = useState({ h: "", v: "", hs: "", g: "" });
  const [imageBase64, setImageBase64] = useState(null);
  const [isUploading, setIsUploading] = useState(false);

  const imageBase64Ref = useRef(null);
  const temp = useRef(null);

  const onFileSelect = (event) => {
    if (event?.currentTarget?.files?.length) {
      temp.current = event.currentTarget.files[0];
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        let img = new Image();
        img.onload = function () {
          if (this.width !== CANVAS_LENGTH && this.height !== CANVAS_LENGTH) {
            setImageBase64(null);
            alert("Invalid image resolution");
          } else {
            setImageBase64(e.target.result);

            const defaultCroppedDataUrl = {};
            const temp = document.createElement("canvas");
            let tempContext = temp.getContext("2d");
            temp.width = sizeMap.h.width;
            temp.height = sizeMap.h.height;
            tempContext.drawImage(this, defaultCrop.h.x, defaultCrop.h.y, sizeMap.h.width, sizeMap.h.height, 0, 0, sizeMap.h.width, sizeMap.h.height);
            defaultCroppedDataUrl.h = temp.toDataURL("image/jpeg", 1.0);

            temp.width = sizeMap.v.width;
            temp.height = sizeMap.v.height;
            tempContext.clearRect(0, 0, CANVAS_LENGTH, CANVAS_LENGTH);
            tempContext.drawImage(this, defaultCrop.v.x, defaultCrop.v.y, sizeMap.v.width, sizeMap.v.height, 0, 0, sizeMap.v.width, sizeMap.v.height);
            defaultCroppedDataUrl.v = temp.toDataURL("image/jpeg", 1.0);

            temp.width = sizeMap.hs.width;
            temp.height = sizeMap.hs.height;
            tempContext.clearRect(0, 0, CANVAS_LENGTH, CANVAS_LENGTH);
            tempContext.drawImage(this, defaultCrop.hs.x, defaultCrop.hs.y, sizeMap.hs.width, sizeMap.hs.height, 0, 0, sizeMap.hs.width, sizeMap.hs.height);
            defaultCroppedDataUrl.hs = temp.toDataURL("image/jpeg", 1.0);

            temp.width = sizeMap.g.width;
            temp.height = sizeMap.g.height;
            tempContext.clearRect(0, 0, CANVAS_LENGTH, CANVAS_LENGTH);
            tempContext.drawImage(this, defaultCrop.g.x, defaultCrop.g.y, sizeMap.g.width, sizeMap.g.height, 0, 0, sizeMap.g.width, sizeMap.g.height);
            defaultCroppedDataUrl.g = temp.toDataURL("image/jpeg", 1.0);

            setCroppedDataUrl(defaultCroppedDataUrl);
          }
        }
        img.onerror = () => {
          setImageBase64(null);
          alert("Not a image file");
        }
        img.src = e.target.result;

      }
      fileReader.readAsDataURL(event.currentTarget.files[0]);
    } else {
      setImageBase64(null);
    }
  }

  const onCrop = (dataUrl) => {
    setCroppedDataUrl({ ...croppedDataUrl, [size]: dataUrl });
  };

  const onClickCroppedImages = (_id) => {
    setSize(_id);
  }

  const onClickUpload = async () => {
    setIsUploading(true);

    const data = {};
    for (let key in croppedDataUrl) {
      await fetch(croppedDataUrl[key])
        .then(res => res.blob())
        .then(img => data[key] = img);
    }

    const promises = Object.keys(data).map((key) => {
      return fetch("https://photoslibrary.googleapis.com/v1/uploads", {
        method: "POST",
        body: data[key],
        headers: uploadHeaders
      });
    })

    let responseTokens = [];
    let albumDetails;
    await Promise.all(promises)
      .then(async (values) => {
        const res = values.filter(i => i.status === 200);
        if (res.length !== 4) {
          throw Error("Failed to upload");
        }

        for (let item of values) {
          await item.text().then(res => responseTokens.push(res));
        }
        return fetch("https://photoslibrary.googleapis.com/v1/albums", {
          method: "POST",
          headers: authoricationHeader,
          body: JSON.stringify({
            album: {
              title: uuidv4()
            }
          })
        })
      })
      .then(response => response.json())
      .then((response) => {
        albumDetails = response;
        if (response.error) {
          throw new Error("Failed to create album");
        }
        return fetch("https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate", {
          method: "POST",
          body: JSON.stringify({
            albumId: albumDetails.id,
            newMediaItems: responseTokens.map((item) => {
              return {
                description: "photo",
                simpleMediaItem: {
                  fileName: uuidv4(),
                  uploadToken: item
                }
              }
            })
          }),
          headers: authoricationHeader
        })
      })
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw new Error("Failed to create metadata");
        }
        props.history.push("/photo", { albumId: albumDetails.id })
      })
      .catch((error) => {
        console.error(error);
        alert(error);
        setIsUploading(false);
      });
  }

  return (
    <div>
      <NavBar />
      <p>Select photos to upload. Photo must be of 1024x1024 resolution.</p>
      <p>For best results, for each image type crop to almost same size or almost same aspect ratio.</p>

      {imageBase64 && <div className="image-edit-container">
        <CropImage
          imageBase64={imageBase64}
          size={size}
          onCrop={onCrop} />
        <div className="cropped-images">
          <ImageComponent
            className="cropped-images-horizontal"
            _id="h"
            image={imageBase64Ref.current}
            size="horizontal"
            width={755}
            height={450}
            onClick={onClickCroppedImages}
            selected={size === "h"}
            croppedDataUrl={croppedDataUrl.h} />
          <ImageComponent
            className="cropped-images-vertical"
            _id="v"
            image={imageBase64Ref.current}
            size="vertical"
            width={365}
            height={450}
            onClick={onClickCroppedImages}
            selected={size === "v"}
            croppedDataUrl={croppedDataUrl.v} />
          <ImageComponent
            className="cropped-images-horizontal-small"
            _id="hs"
            image={imageBase64Ref.current}
            size="horizontal-small"
            width={365}
            height={212}
            onClick={onClickCroppedImages}
            selected={size === "hs"}
            croppedDataUrl={croppedDataUrl.hs} />
          <ImageComponent
            className="cropped-images-gallery"
            size="gallery"
            _id="g"
            image={imageBase64Ref.current}
            width={380}
            height={380}
            onClick={onClickCroppedImages}
            selected={size === "g"}
            croppedDataUrl={croppedDataUrl.g} />
        </div>
        <div className="image-preview">
          <p>Preview</p>
          <img className="selected-cropped-images-preview" src={croppedDataUrl[size]} alt="some p" />
        </div>
      </div>}
      <div className="choose-file-button-container"><FileInput _id="photo-upload" onChange={onFileSelect} /></div>
      {imageBase64 && <button className="button upload-button" onClick={onClickUpload} disabled={isUploading}>Upload</button>}
    </div >
  );
}