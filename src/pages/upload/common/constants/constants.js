export const sizeMap = {
  h: { width: 755, height: 450 },
  v: { width: 365, height: 450 },
  hs: { width: 365, height: 212 },
  g: { width: 380, height: 380 }
};

export const defaultCrop = {
  h: { x: 134, y: 287 },
  v: { x: 329, y: 287 },
  hs: { x: 329, y: 406 },
  g: { x: 322, y: 322 }
};
// (1024 - width and height)/2

export const CANVAS_LENGTH = 1024;