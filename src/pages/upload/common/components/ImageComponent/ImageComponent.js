import React from 'react';
import "./imageComponent.scss";

export const ImageComponent = ({ _id, className, image, size, width, height, onClick, selected, croppedDataUrl }) => {

  const _onClick = (e) => {
    onClick(_id);
  }

  return (
    <div className={`image-component ${className} ${selected ? "selected" : ""}`} onClick={_onClick}>
      <div className="image-component-container">
        <img className="image-component-image" src={croppedDataUrl || image} alt="some p" />
      </div>
      <p className="image-component-size-info">
        {`${size} ${width}x${height}`}
      </p>
    </div>
  )
}