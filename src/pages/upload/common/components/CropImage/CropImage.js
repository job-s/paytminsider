import React, { useRef, useEffect } from "react";

import { sizeMap, CANVAS_LENGTH } from "../../constants/constants";

import "./cropImage.scss"

const CROP_GUIDE_OFFSET = 10;

export const CropImage = ({ imageBase64, size, onCrop }) => {
  const canvasRef = useRef(null);
  const context = useRef(null);
  const coordinates = useRef({ starting: {} });
  const cropGuideRef = useRef(null);
  const cropSizeGuideRef = useRef({});

  useEffect(() => {
    context.current = canvasRef.current.getContext("2d");
  }, []);

  let img = new Image();
  img.onload = function () {
    if (context.current) {
      context.current.drawImage(this, 0, 0);
    }
  }
  img.src = imageBase64;

  const onMouseDown = (e) => {
    coordinates.current.boundaryClientRect = canvasRef.current.getBoundingClientRect();
    const x = e.pageX - (window.pageXOffset + coordinates.current.boundaryClientRect.left);
    const y = e.pageY - (window.pageYOffset + coordinates.current.boundaryClientRect.top);

    coordinates.current.starting = { x, y };

    cropGuideRef.current.style.transform = `translate(${x}px, ${y}px)`;

    cropSizeGuideRef.current.style.visibility = `visible`;
    cropSizeGuideRef.current.style.transform = `translate(${x + CROP_GUIDE_OFFSET}px, ${y + CROP_GUIDE_OFFSET}px)`;
    cropSizeGuideRef.current.innerHTML = `${0}, ${0}`;
  }

  const onMouseMove = (e) => {
    if (!Object.keys(coordinates.current.starting).length) {
      return;
    }

    coordinates.current.boundaryClientRect = canvasRef.current.getBoundingClientRect();
    let x = e.pageX - (window.pageXOffset + coordinates.current.boundaryClientRect.left);
    let y = e.pageY - (window.pageYOffset + coordinates.current.boundaryClientRect.top);

    let width = Math.abs(x - coordinates.current.starting.x);
    let height = Math.abs(y - coordinates.current.starting.y);

    if (x < coordinates.current.starting.x || y < coordinates.current.starting.y) {
      if (x < coordinates.current.starting.x && y >= coordinates.current.starting.y) {
        cropGuideRef.current.style.transform = `translate(${x}px, ${coordinates.current.starting.y}px)`;
      } else if (y < coordinates.current.starting.y && x >= coordinates.current.starting.x) {
        cropGuideRef.current.style.transform = `translate(${coordinates.current.starting.x}px, ${y}px)`;
      } else {
        cropGuideRef.current.style.transform = `translate(${x}px, ${y}px)`;
      }

      cropSizeGuideRef.current.style.transform = `translate(${x - CROP_GUIDE_OFFSET - 55}px, ${y - CROP_GUIDE_OFFSET - 8}px)`;
    } else {
      cropGuideRef.current.style.transform = `translate(${coordinates.current.starting.x}px, ${coordinates.current.starting.y}px)`;

      cropSizeGuideRef.current.style.transform = `translate(${x + CROP_GUIDE_OFFSET}px, ${y + CROP_GUIDE_OFFSET}px)`;
    }

    cropGuideRef.current.style.width = width + "px";
    cropGuideRef.current.style.height = height + "px";

    cropSizeGuideRef.current.innerHTML = `${width * 2}, ${height * 2}`;
  }

  const onMouseUp = (e) => {
    if (!Object.keys(coordinates.current.starting).length) {
      return;
    }

    coordinates.current.boundaryClientRect = canvasRef.current.getBoundingClientRect();
    let x = e.pageX - (window.pageXOffset + coordinates.current.boundaryClientRect.left);
    let y = e.pageY - (window.pageYOffset + coordinates.current.boundaryClientRect.top);

    let width = Math.abs(x - coordinates.current.starting.x);
    let height = Math.abs(y - coordinates.current.starting.y);

    if (x < coordinates.current.starting.x || y < coordinates.current.starting.y) {
      if (x < coordinates.current.starting.x && y >= coordinates.current.starting.y) {
        coordinates.current.starting = { x, y: coordinates.current.starting.y };
      } else if (y < coordinates.current.starting.y && x >= coordinates.current.starting.x) {
        coordinates.current.starting = { x: coordinates.current.starting.x, y };
      } else {
        coordinates.current.starting = { x, y };
      }
    }

    const rect = {
      x: coordinates.current.starting.x * 2,
      y: coordinates.current.starting.y * 2,
      width: width * 2,
      height: height * 2
    }

    coordinates.current.boundaryClientRect = {};
    coordinates.current.starting = {};

    cropGuideRef.current.style.width = 0;
    cropGuideRef.current.style.height = 0;

    cropSizeGuideRef.current.style.visibility = `hidden`;

    if (width === 0 || height === 0) {
      return;
    }

    const output = document.createElement("canvas");
    output.width = sizeMap[size].width;
    output.height = sizeMap[size].height;

    const outputContext = output.getContext("2d");
    let img = new Image();
    img.onload = function () {
      outputContext.drawImage(this, rect.x, rect.y, rect.width, rect.height, 0, 0, sizeMap[size].width, sizeMap[size].height);
      const dataUrl = output.toDataURL("image/jpeg", 1.0);
      onCrop(dataUrl);
    }
    img.src = imageBase64;
  }

  return (
    <div className="crop-image-container"
      onMouseDown={onMouseDown}
      onMouseMove={onMouseMove}
      onMouseUp={onMouseUp}>
      <canvas
        className="crop-image-canvas"
        ref={canvasRef}
        width={CANVAS_LENGTH}
        height={CANVAS_LENGTH}
      ></canvas>
      <div className="crop-image-guide" ref={cropGuideRef}></div>
      <div className="crop-image-size-guide" ref={cropSizeGuideRef}></div>
    </div>
  );
}