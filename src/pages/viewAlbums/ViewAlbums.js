import React, { useEffect, useState } from 'react';
import { NavBar } from "../../common/components/NavBar/NavBar";
import { authoricationHeader } from "../../common/utils/headers"
import "./viewAlbums.scss";

export const ViewAlbums = (props) => {

  const [albums, setAlbums] = useState([]);

  useEffect(() => {
    fetch("https://photoslibrary.googleapis.com/v1/albums", {
      headers: authoricationHeader
    }).then(response => response.json())
      .then(response => setAlbums(response.albums || []));
  }, []);

  const onClickCover = (albumId) => {
    props.history.push("/photo", { albumId });
  }

  return (
    <div>
      <div><NavBar /></div>
      <div>
        <p>Click on cover photo to see all sizes</p>
        <div className="albums  ">
          {
            albums.map((item, index) => {
              return <img key={index} src={item.coverPhotoBaseUrl} alt="album cover" onClick={() => onClickCover(item.id)} />
            })
          }
        </div>
      </div>
    </div>
  );
}

