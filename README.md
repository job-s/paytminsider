### To use ur own google apis
* set your own Client ID from google into REACT_APP_CLIENT_ID in .env file

### Using my google apis
* Authorized URL is "http://localhost:3000"
* Do not change the port number

### To install dependencies
* yarn install

### To run the app
* yarn start

### If you cant connect to google OR if there is a problem with the access_token OR there is a CORS error, then please clear the "localStorage", this will "logout", there is no "logout" button. Or wait for some time.
